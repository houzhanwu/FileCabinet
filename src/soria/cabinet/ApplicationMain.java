package soria.cabinet;

import com.formdev.flatlaf.FlatDarculaLaf;
import soria.cabinet.gui.views.MainForm;

public class ApplicationMain {
    public static void main(String[] args) {
        //设置主题
        FlatDarculaLaf.setup();
        System.setProperty("flatlaf.useWindowDecorations", "true");
        //运行程序
        MainForm main = new MainForm();
        main.start();
    }
}
