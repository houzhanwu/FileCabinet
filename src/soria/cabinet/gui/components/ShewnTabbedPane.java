package soria.cabinet.gui.components;

import resources.ResHelper;
import soria.cabinet.impl.TabbedPanel;
import soria.cabinet.utils.SFont;

import javax.swing.*;
import java.awt.*;

/**
 * 主窗口选项卡
 *  @author Shewn
 */
public class ShewnTabbedPane implements TabbedPanel {
    public static JTabbedPane tabbedPane = new JTabbedPane();
    private static final ImageIcon cabinetIcon = new ImageIcon(ResHelper.getResource("cabinet.png"));
    private static final ImageIcon encryptIcon = new ImageIcon(ResHelper.getResource("encrypt.png"));
    private static final ImageIcon assistanceIcon = new ImageIcon(ResHelper.getResource("assistance.png"));

    static {
        //设置每个选项的标题，图片，链接的组件
        tabbedPane.addTab("文件柜",cabinetIcon,cabinet);
        tabbedPane.addTab("加密解密",encryptIcon,encrypt);
        tabbedPane.addTab("帮助",assistanceIcon,assistance);
        //设置字体，前景色，选项卡大小
        tabbedPane.setFont(SFont.TITLE);
        tabbedPane.setForeground(new Color(0xF6BEC8));
        tabbedPane.setBounds(0,0,850,40);
    }
}
