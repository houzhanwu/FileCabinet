package soria.cabinet.gui.components.cabinet;

import soria.cabinet.utils.CabinetComponents;
import soria.cabinet.utils.SFile;
import soria.cabinet.utils.SFont;

import javax.swing.*;
import java.io.File;

//文件信息
public class FileInfoPosition {
    //Panel
    public static JPanel panel = new JPanel();

    private static String fileName = "";

    public FileInfoPosition(String fileName) {
        FileInfoPosition.fileName = fileName;
        initial();
    }

    static {
        panel.setLayout(null);
        panel.setBounds(215, 5, 615, 520);
        panel.setBorder(BorderFactory.createLoweredBevelBorder());
        panelAdd(panel);
    }

    private void initial() {
        //获取文件图标和文件名
        ImageIcon imageIcon = SFile.getFileImage(SFile.getFileExtension(fileName));
        FileIconAndName.icon.setIcon(imageIcon);
        FileIconAndName.name.setText(fileName);
        FileIconAndName.name.setVisible(true);

        FileInfo.type.setText("文件类型：" +SFile.getFileExtension(fileName)+" 文件");
        FileInfo.position.setText("位置：" + new File(SFile.getPath(fileName)).getParentFile());
        FileInfo.size.setText("大小：" + SFile.getSize(fileName));
        FileInfo.create.setText("创建时间：" + SFile.getCreateTime(fileName));
        FileInfo.time.setText("最后修改时间：" + SFile.getModifyTime(fileName));
        FileInfo.type.setVisible(true);
        FileInfo.separator.setVisible(true);
        FileInfo.position.setVisible(true);
        FileInfo.size.setVisible(true);
        FileInfo.create.setVisible(true);
        FileInfo.time.setVisible(true);
    }

    //文件图标和文件名
    private static class FileIconAndName {
        public static JLabel icon = new JLabel();
        public static JTextField name = new JTextField(fileName);

        static {
            icon.setBounds(15, 15, 64, 64);

            name.setVisible(false);
            name.setEditable(false);
            name.setBounds(85, 31, 500, 32);
            name.setFont(SFont.TEXT);
        }
    }

    //文件的其他信息（文件位置、文件大小、文件最后修改时间）
    private static class FileInfo {
        public static JLabel type = new JLabel("文件类型：");
        public static JSeparator separator = new JSeparator();
        public static JLabel position = new JLabel("位置：");
        public static JLabel size = new JLabel("大小：");
        public static JLabel create = new JLabel("创建时间：");
        public static JLabel time = new JLabel("最后修改时间：");

        static {
            CabinetComponents.initInfo(type,100);
            CabinetComponents.initInfo(position,150);
            CabinetComponents.initInfo(size,190);
            CabinetComponents.initInfo(create,230);
            CabinetComponents.initInfo(time,270);
            separator.setBounds(15,140,565,5);
            separator.setVisible(false);
        }
    }

    //添加所有组件到Panel面板中
    private static void panelAdd(JPanel panel) {
        panel.add(FileIconAndName.icon);
        panel.add(FileIconAndName.name);
        panel.add(FileInfo.type);
        panel.add(FileInfo.separator);
        panel.add(FileInfo.position);
        panel.add(FileInfo.size);
        panel.add(FileInfo.create);
        panel.add(FileInfo.time);
    }
}

