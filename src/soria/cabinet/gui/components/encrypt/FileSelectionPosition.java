package soria.cabinet.gui.components.encrypt;

import soria.cabinet.utils.EncryptComponents;
import soria.cabinet.utils.SFont;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FileSelectionPosition {
    //Panel
    public static JPanel panel = new JPanel();
    //待加密文件区域标题
    private static final JLabel encryptTitle = EncryptComponents.createLabel("待加密文件");
    //待解密文件区域标题
    private static final JLabel decryptTitle = EncryptComponents.createLabel("待解密文件");
    //解密所需密钥标题
    private static final JLabel secretKeyTitle = EncryptComponents.createLabel("解密所需密钥：");
    //密钥输入框
    protected static JTextField secretKeyText = new JTextField();
    //待加密文件列表
    protected static JList<String> encryptList = new JList<>(new DefaultListModel<>());
    //待解密文件列表
    protected static JList<String> decryptList = new JList<>(new DefaultListModel<>());

    static {
        EncryptComponents.initialPanel(panel);
        panel.setBounds(5, 5, 600, 517);
        initial();
    }

    //初始化面板
    private static void initial() {
        encryptTitle.setBounds(100, 10, 100, 30);
        panel.add(encryptTitle);

        decryptTitle.setBounds(400, 10, 100, 30);
        panel.add(decryptTitle);

        secretKeyTitle.setBounds(20, 460, 130, 30);
        panel.add(secretKeyTitle);

        secretKeyText.setBounds(160, 460, 423, 30);
        secretKeyText.setFont(SFont.DEFAULT_EN);
        panel.add((secretKeyText));

        encryptList.setBounds(20, 40, 270, 400);
        encryptList.setFont(SFont.TEXT);
        encryptList.addMouseListener(new CancelListener(encryptList));
        panel.add(encryptList);

        decryptList.setBounds(310, 40, 270, 400);
        decryptList.setFont(SFont.TEXT);
        decryptList.addMouseListener(new CancelListener(decryptList));
        panel.add(decryptList);
    }

    //事件监听器：右键取消选中
    private static class CancelListener extends MouseAdapter {
        private final JList<String> list;
        public CancelListener(JList<String> list) {
            this.list = list;
        }
        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON3) {
                int selectIndex = list.locationToIndex(e.getPoint());
                if(selectIndex != -1){
                    list.getSelectionModel().removeSelectionInterval(selectIndex,selectIndex);
                }
            }
        }
    }
}
