package soria.cabinet.gui.components.assistance;

import soria.cabinet.utils.SFont;

import javax.swing.*;

public class TipPosition {
    public static JPanel panel = new JPanel();
    private static final JLabel encryptIdea = new JLabel("1.如何加密文件？");
    private static final String encryptString = "切换到“加密解密”界面，点击“选择待加密文件”按钮，选择需要加密的文件,再点击“加密文件”按钮。" +
            "在“帮助”界面将对方的公钥复制到文本框内，并点击“获取密钥”按钮，将生成的密钥一并发送给对方即可。";
    private static final JTextArea encryptArea = new JTextArea(encryptString);
    private static final JLabel decryptIdea = new JLabel("2.如何解密文件？");
    private static final String decryptString = "切换到“加密解密”界面，点击“选择待解密文件”按钮，选择需要解密的文件，" +
            "并将对方发送给你的密钥输入到密钥框，点击“解密文件”按钮即可完成解密。";
    private static final JTextArea decryptArea = new JTextArea(decryptString);
    private static final JLabel ownIdea = new JLabel("3.其他");
    private static final String ownString = "如果使用自己的公钥加密自己的文件，则直接点击“获取密钥”按钮，并将获取到的密钥粘贴到密钥输入框解密即可。";
    private static final JTextArea ownArea = new JTextArea(ownString);
    private static final JLabel copyright1 = new JLabel("Copyright ©2023-2024 ShewnGeung");
    private static final JLabel copyright2 = new JLabel("All Rights Reserved");

    static {
        panel.setLayout(null);
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), "说明", 2, 2, SFont.TITLE));
        panel.setBounds(15, 7, 390, 508);
        initial();
    }

    private static void initial() {
        addLabel(encryptIdea, 20);
        addArea(encryptArea, 45, 120);
        addLabel(decryptIdea, 170);
        addArea(decryptArea, 195, 100);
        addLabel(ownIdea, 300);
        addArea(ownArea, 320, 130);

        copyright1.setBounds(35,460,370, 20);
        copyright1.setFont(SFont.DEFAULT_EN);
        panel.add(copyright1);
        copyright2.setBounds(95,480,370, 20);
        copyright2.setFont(SFont.DEFAULT_EN);
        panel.add(copyright2);
    }

    /**
     * 添加label到panel中
     *
     * @param label 要添加的label
     * @param y     y坐标
     */
    private static void addLabel(JLabel label, int y) {
        label.setBounds(10, y, 370, 20);
        label.setFont(SFont.DEFAULT_CN);
        panel.add(label);
    }

    /**
     * 添加textarea到panel中
     *
     * @param area   要添加的JTextArea
     * @param y      y坐标
     * @param height 组件高度
     */
    private static void addArea(JTextArea area, int y, int height) {
        area.setBounds(10, y, 370, height);
        area.setFont(SFont.TEXT);
        area.setEditable(false);
        area.setLineWrap(true);
        panel.add(area);
    }
}
