package soria.cabinet.gui.views;

import soria.cabinet.gui.components.cabinet.FileInfoPosition;
import soria.cabinet.gui.components.cabinet.FileTreePosition;
import soria.cabinet.impl.TabbedPanel;

public class CabinetForm implements TabbedPanel {
    static {
        cabinet.setLayout(null);
        cabinet.add(FileTreePosition.panel);
        cabinet.add(FileInfoPosition.panel);
    }
}
