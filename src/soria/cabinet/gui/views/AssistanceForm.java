package soria.cabinet.gui.views;

import soria.cabinet.gui.components.assistance.KeyPosition;
import soria.cabinet.gui.components.assistance.TipPosition;
import soria.cabinet.impl.TabbedPanel;

public class AssistanceForm implements TabbedPanel {
    static {
        assistance.setLayout(null);
        assistance.add(TipPosition.panel);
        assistance.add(KeyPosition.panel);
    }
}