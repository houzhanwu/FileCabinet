package soria.cabinet.gui.views;

import resources.ResHelper;
import soria.cabinet.gui.components.ShewnTabbedPane;

import javax.swing.*;
import java.io.File;

public class MainForm{
    //主窗口
    JFrame mainFrame = new JFrame("ShewnCabinet");

    public MainForm(){
        //程序图标
        ImageIcon mainIcon = new ImageIcon(ResHelper.getResource("main.png"));
        mainFrame.setIconImage(mainIcon.getImage());
        //主窗口相关配置
        mainFrame.setSize(850,600);
        mainFrame.setResizable(false);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(mainFrame.EXIT_ON_CLOSE);
        mainFrame.setVisible(false);
    }

    /**
     * 启动程序
     */
    public void start(){
        //创建文件柜位置
        File cabinet = new File(new File("").getAbsolutePath()+"/Cabinet");
        //noinspection ResultOfMethodCallIgnored
        cabinet.mkdir();
        //添加选项卡组件
        mainFrame.add(ShewnTabbedPane.tabbedPane);
        new CabinetForm();
        new EncryptForm();
        new AssistanceForm();
        mainFrame.setVisible(true);
    }
}
