package soria.cabinet.gui.views;

import soria.cabinet.gui.components.encrypt.FileSelectionPosition;
import soria.cabinet.gui.components.encrypt.OperatePosition;
import soria.cabinet.impl.TabbedPanel;

public class EncryptForm implements TabbedPanel {
    static {
        encrypt.setLayout(null);
        encrypt.add(FileSelectionPosition.panel);
        encrypt.add(OperatePosition.panel);
    }
}
