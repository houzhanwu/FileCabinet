package soria.cabinet.utils;

import javax.swing.*;

public class EncryptComponents {
    /**
     * 初始化设置选项中各个选项所在的Panel
     * @param panel Panel
     */
    public static void initialPanel(JPanel panel){
        panel.setLayout(null);
        panel.setBorder(BorderFactory.createLoweredBevelBorder());
    }
    public static JLabel createLabel(String text) {
        JLabel label = new JLabel(text);
        label.setFont(SFont.TITLE);
        return label;
    }

    public static JButton createBtn(String text,int y) {
        JButton btn = new JButton(text);
        btn.setBounds(25,y,170,40);
        btn.setFont(SFont.DEFAULT_CN);
        return btn;
    }
}
