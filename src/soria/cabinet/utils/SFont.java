package soria.cabinet.utils;

import resources.ResHelper;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

public class SFont {
    public static final Font DEFAULT_CN;
    public static final Font DEFAULT_EN;
    public static final Font TITLE;
    public static final Font TEXT;

    static {
        DEFAULT_CN = quoteFont("三极锦宋简.ttf", 1, 17);
        DEFAULT_EN = quoteFont("JetBrainsMono.ttf", 0, 17);
        TITLE = quoteFont("汉仪文黑-85W.ttf", 0, 18);
        TEXT = quoteFont("苍耳渔阳.ttf", 0, 17);
    }

    /**
     * 引入自定义字体
     * @param fontName 字体名
     * @param style 字体风格
     * @param size 字体大小
     * @return Font
     */
    private static Font quoteFont(String fontName, int style, int size){
        try {
            InputStream input = ResHelper.getResource(fontName).openStream();
            Font customFont = Font.createFont(Font.TRUETYPE_FONT,input);
            customFont = customFont.deriveFont(style, size);
            input.close();
            return customFont;
        } catch (IOException | FontFormatException e) {
            throw new RuntimeException(e);
        }
    }
}
