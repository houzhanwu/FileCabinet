package soria.cabinet.utils;

import resources.ResHelper;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SFile {
    private static final Map<String, String> fileMap = new HashMap<>();
    private static File file;

    /**
     * 显示文件柜目录下所有的子目录和文件，包括子目录中的目录和文件
     *
     * @param root      目录根结点
     * @param directory 根目录
     */
    public static void viewAll(DefaultMutableTreeNode root, File directory) {
        String[] list = directory.list();
        DefaultMutableTreeNode node;
        File file;
        if (list != null) {
            for (String s : list) {
                node = new DefaultMutableTreeNode(s);
                file = new File(directory, s);
                root.add(node);
                if (file.isDirectory()) {
                    viewAll(node, file);
                } else {
                    //记录文件名和文件路径
                    fileMap.put(s, file.getAbsolutePath());
                }
            }
        }
    }

    /**
     * 获取文件大小
     *
     * @param fileName 文件名
     * @return 文件大小，小于1024字节返回字节大小，小于1024KB返回KB大小，小于1024MB返回MB大小
     */
    public static String getSize(String fileName) {
        file = new File(fileMap.get(fileName));
        StringBuilder size = new StringBuilder();
        double fileSize = file.length();
        if (fileSize < 1024) {
            size.append((int) fileSize).append(" 字节");
        } else if (fileSize < 1024 * 1024) {
            size.append(String.format("%.2f", fileSize / 1024)).append(" KB");
        } else if (fileSize < 1024 * 1024 * 1024) {
            size.append(String.format("%.2f", fileSize / (1024 * 1024))).append(" MB");
        }
        size.append("(").append((int) fileSize).append(" 字节)");
        return size.toString();
    }

    /**
     * 获取文件路径
     *
     * @param fileName 文件名
     * @return 文件路径
     */
    public static String getPath(String fileName) {
        if (fileMap.containsKey(fileName))
            return fileMap.get(fileName);
        return "";
    }

    /**
     * 获取创建文件的时间
     * @param fileName 文件名
     * @return 创建时间
     */
    public static String getCreateTime(String fileName){
        try {
            File f = new File(fileMap.get(fileName));
            BasicFileAttributes attr = Files.readAttributes(f.toPath(),BasicFileAttributes.class);
            FileTime time = attr.creationTime();
            return new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss").format(new Date(time.toMillis()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取修改文件的最后时间
     *
     * @param fileName 文件名
     * @return 最后修改时间
     */
    public static String getModifyTime(String fileName) {
        file = new File(fileMap.get(fileName));
        return new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss").format(new Date(file.lastModified()));
    }

    /**
     * 获取文件扩展名
     *
     * @param fileName 文件名
     * @return 文件扩展名
     */
    public static String getFileExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf('.');
        return dotIndex > 0 ? fileName.substring(dotIndex + 1) : "";
    }

    /**
     * 根据扩展名获取文件图标
     *
     * @param extension 文件扩展名
     * @return 内置了常用的文件对应的图标，未获取到对应图标则返回未知文件图标
     */
    public static ImageIcon getFileImage(String extension) {
        ImageIcon imageIcon;
        switch (extension) {
            case "mp3", "wav", "ogg", "flac" -> imageIcon = new ImageIcon(ResHelper.getResource("music_file.png"));
            case "mp4", "avi", "mov", "mkv" -> imageIcon = new ImageIcon(ResHelper.getResource("video_file.png"));
            case "jpg", "png", "jpeg" -> imageIcon = new ImageIcon(ResHelper.getResource("image_file.png"));
            case "doc", "docx" -> imageIcon = new ImageIcon(ResHelper.getResource("word_file.png"));
            case "xls", "xlsx" -> imageIcon = new ImageIcon(ResHelper.getResource("excel_file.png"));
            case "ppt", "pptx" -> imageIcon = new ImageIcon(ResHelper.getResource("ppt_file.png"));
            case "pdf" -> imageIcon = new ImageIcon(ResHelper.getResource("pdf_file.png"));
            case "txt" -> imageIcon = new ImageIcon(ResHelper.getResource("txt_file.png"));
            default -> imageIcon = new ImageIcon(ResHelper.getResource("unknown_file.png"));
        }
        return imageIcon;
    }
}
