package soria.cabinet.impl;

import javax.swing.*;

public interface TabbedPanel {
    //文件柜Panel
    JPanel cabinet = new JPanel();
    //加密解密Panel
    JPanel encrypt = new JPanel();
    //帮助Panel
    JPanel assistance = new JPanel();
}
