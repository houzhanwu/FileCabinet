package resources;

import java.net.URL;

public class ResHelper {
    /**
     * 从资源包中加载资源
     * @param fileName 资源名
     * @return URL
     */
    public static URL getResource(String fileName) {
        if(fileName.endsWith(".png")){
            return ResHelper.class.getResource("images/" + fileName);
        }else {
            return ResHelper.class.getResource("fonts/" + fileName);
        }
    }
}
